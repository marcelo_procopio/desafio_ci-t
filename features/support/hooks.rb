Before('@autenticado') do 
    @login_page = LoginPage.new
    @login_page.load
    @login_page.logar('marcelo.procopio@yopmail.com', 'senha123')

end

Before('@produto_no_carrinho') do 
    @busca = BuscaPage.new
    @busca.wait_for_campo_busca
    @busca.busca_simples('iPhone X Prata 64GB')
    @busca.seleciona_primeiro
    @busca.adiciona_carrinho.click

end
