class BuscaPage < SitePrism::Page
    element :campo_busca, 'input[id=suggestion-search]'
    element :botao_lupa, 'button[class*=search-icon]'
    elements :clica_produto, "li[class*=product-item]"
    element :adiciona_carrinho, 'button[class*="right buy-button"]'
    element :titulo, 'h1[class="product-name"]'

    def busca_simples(produto)
        campo_busca.set produto
        botao_lupa.click
    end

    def seleciona_primeiro
        clica_produto[0].click
    end

end
