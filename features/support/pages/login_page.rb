class LoginPage < SitePrism::Page
    set_url '/login'
    element :campo_email, 'input[name=signinField]'
    element :campo_senha, 'input[name=password]'
    element :botao_entrar, '#signinButtonSend'
    element :avatar, 'div[class="profile"]'

    def logar(email, senha)
        campo_email.set email
        campo_senha.set senha
        botao_entrar.click
    end



end

