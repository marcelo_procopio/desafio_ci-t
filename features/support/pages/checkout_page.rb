class CheckoutPage < SitePrism::Page

    element :qtd, 'input[class*=ui-spinner-input]'
    element :aumenta_qtd, 'a[class*=ui-spinner-up'
    element :remover, 'a[class=link-trash]'
    element :finalizar, '#btn-finalize-cart'
    element :continuar, "button[class='btn btn-success']"

end