class PaymentPage < SitePrism::Page

    element :cartao_credito, 'i[class*=credit-card]'
    element :numero_cartao, '#txtCardNumber'
    element :nome_cartao, '#txtName'
    element :validade_cartao, '#expirationDate'
    element :cod_seguranca, '#txtSecurityCode'
    element :cpf_cartao, '#txtDocument'
    
    
    elements :selecionar_num_parcelas, 'ul[class*=dropdown-menu] li a'
    element  :parcelas, 'div[class*=filter-option]'
    
    def selecionar_parcelas(qtd_parcelas)
        parcelas.click
        selecionar_num_parcelas[qtd_parcelas].click
    end

    def inserir_dados(numero_cartao,nome_cartao,validade_cartao,cod_seguranca,cpf_cartao,numero_parcelas)
        self.numero_cartao.set numero_cartao 
        wait_for_nome_cartao
        self.nome_cartao.set nome_cartao
        self.validade_cartao.set validade_cartao
        self.cod_seguranca.set cod_seguranca
        wait_for_cpf_cartao
        self.cpf_cartao.set cpf_cartao
       
        selecionar_parcelas(numero_parcelas.to_i)

    end 
    

end