Dado("que eu tenha produtos no carrinho") do
    visit 'checkout/content/carrinho/'
    expect(page).to have_content 'Item(s)'
    @checkout = CheckoutPage.new
    @pagamento = PaymentPage.new
end
  
Quando("clico para aumentar a quantidade de produtos") do
    quantidade = @checkout.qtd.text
    @checkout.aumenta_qtd.click
end
    
Quando("clico para finalizar compra") do
    @checkout.finalizar.click
end
  
Então("sou redirecionado para a página de compra") do
    @checkout.continuar.click
    @checkout.wait_for_continuar
    @checkout.continuar.click
end
  
Quando("clico para remover o produto do carrinho") do
    @checkout.remover.click
end
  
Então("o produto é removido do carrinho") do
    expect(page).to have_content 'Seu carrinho está vazio'
end

Então("preencho os dados de pagamento com cartão de crédito") do
  @pagamento.wait_for_cartao_credito
  #dados aleatorios gerados atraves do site www.4devs.com.br
  @pagamento.inserir_dados('4492348773453311','Marcelo CIandT', '04/19','336', '79336829017', '1')
end