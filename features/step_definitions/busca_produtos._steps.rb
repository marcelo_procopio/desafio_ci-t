Dado("que eu busco pelo produto {string}") do |produto|

    @produto_buscado = produto
    @busca = BuscaPage.new
    @busca.wait_for_campo_busca
    @busca.busca_simples(@produto_buscado)
    expect(page).to have_content 'Resultados de busca para'
  end
  
  Quando("o produto é encontrado") do
    @busca.seleciona_primeiro
    expect(@busca.titulo.text).to have_content @produto_buscado
  end

  
  Então("adiciono o produto ao carrinho") do
    @busca.adiciona_carrinho.click
  end
  