Dado("que eu acesso o formulário de login") do
    @login_page = LoginPage.new
    @login_page.load
end
  
Quando("faço login com {string} e {string}") do |email, senha|
    @login_page.logar(email, senha)
end
  
Então("sou antenticado com sucesso") do
    @login_page.avatar.click
    expect(page).to have_content 'Marcelo CIandT'
end
  