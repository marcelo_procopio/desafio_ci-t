#language:pt

@checkout @autenticado @produto_no_carrinho
Funcionalidade: Gerenciar produtos no carrinho
    Para que eu possa efetuar compras
    Sendo um usuario logado
    Posso comprar e remover os produtos do carrinho 


    Cenario: Aumentar a quantidade do produto no carrinho
        Dado que eu tenha produtos no carrinho
        Quando clico para aumentar a quantidade de produtos
        E clico para finalizar compra
        Então sou redirecionado para a página de compra
        E preencho os dados de pagamento com cartão de crédito
        
    Cenario: Compra
        Dado que eu tenha produtos no carrinho
        Quando clico para finalizar compra
        Então sou redirecionado para a página de compra
        E preencho os dados de pagamento com cartão de crédito

    Cenario: Remover produtos do carrinho
        Dado que eu tenha produtos no carrinho
        Quando clico para remover o produto do carrinho
        Então o produto é removido do carrinho