#language: pt

@login
Funcionalidade: Login
    Para que eu possa efetuar compras no site
    Sendo um usuario cadastrado
    Possa efetuar login


    Cenário: Login do Usuário
        Dado que eu acesso o formulário de login
        Quando faço login com "marcelo.procopio@yopmail.com" e "senha123"
        Então sou antenticado com sucesso
