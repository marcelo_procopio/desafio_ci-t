#language:pt

@buscar_produtos @autenticado
Funcionalidade: Gerenciamento do carrinho
    Para que eu possa buscar produtos
    Sendo um usuario logado
    Posso adicionar os produtos ao carrinho 


    Cenario: Buscar o produto
        Dado que eu busco pelo produto "iPhone X Prata 64GB"
        Quando o produto é encontrado
        Então adiciono o produto ao carrinho

